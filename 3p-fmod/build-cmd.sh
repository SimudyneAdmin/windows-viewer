#!/bin/sh

cd "$(dirname "$0")" 

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

# Check autobuild is around or fail
if [ -z "$AUTOBUILD" ] ; then 
    fail
fi
if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# Load autobuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

# Form the official fmod archive URL to fetch
# Note: fmod is provided in 3 flavors (one per platform) of precompiled binaries. We do not have access to source code.
FMOD_ROOT_NAME="fmodapi"
FMOD_VERSION="375"
case "$AUTOBUILD_PLATFORM" in
    "windows")
    FMOD_PLATFORM="win"
    FMOD_FILEEXTENSION=".zip"
    FMOD_MD5="4d28a685a92557c0dac06f9ab2567203"
    ;;
    "darwin")
    FMOD_PLATFORM="mac"
    FMOD_FILEEXTENSION=".zip"
    FMOD_MD5="69011586de5725de08c10611b1a0289a"
    ;;
    "linux")
    FMOD_PLATFORM="linux"
    FMOD_FILEEXTENSION=".tar.gz"
    FMOD_MD5="4fbd42fb8187c37ea454cc66186a1dfa"
    ;;
esac
FMOD_SOURCE_DIR="$FMOD_ROOT_NAME$FMOD_VERSION$FMOD_PLATFORM"
FMOD_ARCHIVE="$FMOD_SOURCE_DIR$FMOD_FILEEXTENSION"
FMOD_URL="http://www.fmod.org/files/fmod3/$FMOD_ARCHIVE"

# Fetch and extract the official fmod files
fetch_archive "$FMOD_URL" "$FMOD_ARCHIVE" "$FMOD_MD5"
# Workaround as extract does not handle .zip files (yet)
# TODO: move that logic to the appropriate autobuild script
case "$FMOD_ARCHIVE" in
    *.zip)
        # unzip locally, redirect the output to a local log file
        unzip -n "$FMOD_ARCHIVE" >> "$FMOD_ARCHIVE".unzip-log 2>&1
    ;;
    *.tar.gz)
        extract "$FMOD_ARCHIVE"
    ;;
esac

top="$(pwd)"
stage="$(pwd)/stage"

# Rename to avoid the platform name in the root dir, easier to match autobuild.xml
mv "$FMOD_SOURCE_DIR" "$FMOD_ROOT_NAME$FMOD_VERSION"

pushd "$FMOD_ROOT_NAME$FMOD_VERSION"
    case "$AUTOBUILD_PLATFORM" in
        "windows")
			# Create the staging folders
            mkdir -p "$stage/lib"/{debug,release}
            # Copy relevant stuff around: renaming the import lib to make it easier on cmake
            cp "api/lib/fmodvc.lib" "$stage/lib/debug/fmod.lib"
            cp "api/lib/fmodvc.lib" "$stage/lib/release/fmod.lib"
            cp "api/fmod.dll" "$stage/lib/debug/fmod.dll"
            cp "api/fmod.dll" "$stage/lib/release/fmod.dll"
        ;;
        "darwin")
            # Create a universal version of the lib for the Mac
            # Note : we do *not* support PPC anymore since Viewer 2 but we leave that here
            # in case we might still need to create universal binaries with fmod in some other project
            lipo -create "api/lib/libfmod.a" "api/lib/libfmodx86.a" -output "api/lib/libfmod.a"
            touch -r "api/lib/libfmodx86.a" "api/lib/libfmod.a"
            # Create a staging folder
            mkdir -p "$stage/lib/release"
            # Copy relevant stuff around
            cp "api/lib/libfmod.a" "$stage/lib/release/libfmod.a"
        ;;
        "linux")
            # Create a staging folder
            mkdir -p "$stage/lib/release"
            # Copy relevant stuff around
            cp "api/libfmod-3.75.so" "$stage/lib/release/libfmod-3.75.so"
        ;;
    esac
	# Create the staging include folder
    mkdir -p "$stage/include"
    # Copy the headers
    cp "api/inc/fmod.h" "$stage/include/fmod.h"
    cp "api/inc/fmod_errors.h" "$stage/include/fmod_errors.h"
    # Copy License (extracted from the readme)
    mkdir -p "$stage/LICENSES"
    tail -n 66 README.TXT > "$stage/LICENSES/fmod.txt"
popd
pass

